import { assert } from 'chai'
import ProductPage from '../../src/models/ProductPage';
import Product from '../../src/models/Product';

const PAGE_ID = 1;
const TOTAL_PAGES = 10;

describe('testing functions productpage class',() => {
    it('test new instance', () => {
        const pp = new ProductPage(PAGE_ID,'','',TOTAL_PAGES);
        assert.strictEqual(pp.totalPages,TOTAL_PAGES);
        assert.strictEqual(pp.number,PAGE_ID);
    });
    it('test products', () => {
        const pp = new ProductPage(PAGE_ID,'','',TOTAL_PAGES);
        const p1 = {
            "id": 918659,
            "variants": [
                {
                    "ean": "9000000888887",
                    "reference": "9000000888887"
                }
            ],
            "name": "batatas24",
            "description": "description 1",
            "supplier": "",
            "season": "Permanent",
            "family": "Top",
            "subfamily": "Jacket",
            "type": "Clothing",
            "fabric": "",
            "currency_iso3code": "EU"
        };
        const p2 =         {
            "id": 918660,
            "variants": [
                {
                    "ean": "9000000888888",
                    "reference": "9000000888888"
                }
            ],
            "name": "batatas25",
            "description": "description 1",
            "supplier": "",
            "season": "Permanent",
            "family": "Top",
            "subfamily": "Jacket",
            "type": "Clothing",
            "fabric": "",
            "currency_iso3code": "EU"
        };
        //frist product
        const product1 = new Product(p1.id);
        product1.loadByData(p1);
        pp.addProduct(product1);

        //secund product
        const product2 = new Product(p2.id);
        product2.loadByData(p2);
        pp.addProduct(product2);

        assert.strictEqual(pp.products.length,2);
        assert.isTrue(product1.equals(pp.getProduct(product1.id)));
        assert.isTrue(product2.equals(pp.getProduct(product2.id)));
    });
});
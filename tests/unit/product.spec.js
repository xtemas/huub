import { assert } from 'chai'
import Product from '../../src/models/Product';

const PRODUCT_ID = 1;

describe('testing functions product class',() => {
    it('test new instance', () => {
        const p = new Product(PRODUCT_ID);
        assert.strictEqual(p.id,PRODUCT_ID);
    });
    it('test load data', () => {
        const p = new Product(PRODUCT_ID);
        const obj = {
            "id": PRODUCT_ID,
            "variants": [
                {
                    "ean": "9000000888886",
                    "reference": "9000000888886"
                }
            ],
            "name": "batatas23",
            "description": "description 1",
            "supplier": "",
            "season": "Permanent",
            "family": "Top",
            "subfamily": "Jacket",
            "type": "Clothing",
            "fabric": "",
            "currency_iso3code": "EU"
        };
        p.loadByData(obj);
        assert.strictEqual(p.name,obj.name);
        assert.strictEqual(p.description,obj.description);
        assert.strictEqual(p.fabric,obj.fabric);
        assert.strictEqual(p.currency,obj.currency_iso3code);
        assert.strictEqual(p.variants.length,obj.variants.length);
    });
});
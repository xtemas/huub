import { assert } from 'chai'
import Variant from '../../src/models/Variant';

describe('testing functions variant class',() => {
    it('test new instance', () => {
        let ean = 'test1';
        let reference = 'test2';
        let v = new Variant(ean,reference);
        assert.strictEqual(v.ean,ean);
        assert.strictEqual(v.reference,reference);
    });
    it('test properties', () => {
        let test = '123';
        let v = new Variant('ean','reference');
        v.setProperties({
            prop1: test,
            prop2: (test + '-')
        });
        let props = v.getProperties();
        assert.strictEqual(props.get('prop1'),test);
        assert.notEqual(props.get('prop2'),test);
    });
});
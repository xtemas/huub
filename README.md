# HUUB (case study)
This project aims to respond to a challenge posed by Huub.<br />
<br /><br />
Project Requirements:
We would like to see a simple application that answers to the following requirements:<br />
• The user should be able to login to the application;<br />
• The user should be able to see a paginated list of products, which should only displays the product ids and the product names;<br />
• The user should be able to click a product from the list and see its details<br />
(description, season, supplier and a list of variants...)
<br />
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run unit tests
```
yarn run test:unit
```

### External Links
- [Gitlab](https://gitlab.com/xtemas/huub).
- [Live demo](http://huub.sergiomoreira.eu).

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

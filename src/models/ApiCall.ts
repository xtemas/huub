import axios, {AxiosResponse, AxiosRequestConfig, Method} from 'axios';
import ProductPage from './ProductPage';
import Product from './Product';
import Vue from 'vue'
import VueSessionStorage from "vue-sessionstorage";
Vue.use(VueSessionStorage);

const apiSERVER = 'https://api.brand.uat.thehuub.io';
const PERPAGE = 20;

export default {
    /**
     * 
     * @param {string} login 
     * @param {string} pass 
     * @returns Object
     */
    lastError: null,
    login: async function(login: string, pass: string){
        const displayDetails = false;
        const response = await this.call('post','/authenticate',null,{email: login, password: pass});
        if(response.status == 200){
            return response.data.data;
        }
        return {
            error: 'LOGINERROR',
            message: ('data' in response) && response.data && ('details' in response.data) ? (displayDetails ? response.data.details : 'Login data not valid!') : 'Error, please try again!'
        };
    },
    /**
     * 
     * @param {number} pageNumber 
     * @param {string}|null token 
     * @returns {ProductPage}|null
     */
    getPage: async function(pageNumber: number,token:string|null ){
        const { data } = await this.call('get',`/products`,token,{page: pageNumber,page_size: PERPAGE});
        if('error' in data){
            return null;
        }
        if('paginator' in data && 'data' in data ){
            const products  = data.data;
            const paginator = data.paginator;
            const page = new ProductPage(paginator.page_number,paginator.next_page_url,paginator.previous_page_url,paginator.total_pages);
            if(Array.isArray(products)){
                products.map( product => {
                    const p = new Product(product.id);
                    p.loadByData(product);
                    page.addProduct(p);
                });
            }
            return page;
        }
        return null;
    },

    /**
     * 
     * @param {number} id 
     * @param {string}|null token 
     * @returns {Product}| null
     */
    getProduct: async function(id: number,token:string|null): Promise<Product|null>{
        const { data } = await this.call('get',`/products/${id}`,token);
        if('error' in data){
            return null;
        }
        if(!('data' in data) && Array.isArray(data.data) && data.data.length){
            return null;
        }
        const p = new Product(id);
        p.loadByData(data.data[0]);
        return p;
    },
    /**
     * 
     * @param {Method} type 
     * @param {string} point 
     * @param {string}|null token 
     * @param {Object}|null body 
     * @returns {Object}
     */
    call: async function(type:Method, point:string,token:string|null = null,params:any = null): Promise<AxiosResponse<any, any>  | {status: number, data: any}>{
        const config:AxiosRequestConfig<any> = {
            method: type,
            validateStatus: (status) => {return status >= 200 && status < 500;},
            url: apiSERVER + point
        }
        this.lastError = null;
        if(params){
            if(type === 'get'){
                config.params = params;
            }else{
                config.data = params;
            }
        }
        if(token){
            config.headers = {'jwt': token};
        }
        try{
            const rst =  await axios(config);
            if(rst.status == 440){
                //To do: improve this code
                setTimeout(() => {
                    Vue.prototype.$session.clear();
                },5000);
                return {
                    status: rst.status,
                    data:{
                        error: 'SESSIONEXPIRED',
                        message: 'Your session ended!'
                    }
                }
            }
            if('data' in rst && rst.data && 'error' in rst.data){
                this.lastError = rst.data.error;
            }
            return rst;
        }catch(err){
            return {
                status: 500,
                data:{
                    error: 'NETWORK',
                    message: 'Not possible comunicate with server!'
                }
            }
        }
    }
}
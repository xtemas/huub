export default class Variant {
    protected _ean: string;
    protected _reference: string;
    constructor(ean: string, reference: string) {
        this._ean = ean;
        this._reference = reference;
    }

    get ean(): string {
        return this._ean;
    }

    set ean(value: string) {
        this._ean = value;
    }
    
    get reference(): string {
        return this._reference;
    }

    set reference(value: string) {
        this._reference = value;
    }

    /**
     * Return all object properties 
     * @returns Map<string,any>
     */
    public getProperties(): Map<string,any>{
        const rst   = new Map();
        const props = Object.getOwnPropertyNames(this);
        props.forEach( v => {
            switch(v){
                case '_ean':
                    rst.set('ean',this.ean);
                    break;
                case '_reference':
                    rst.set('reference',this.reference);
                    break;
                default:
                    if(!v.startsWith('_')){
                        rst.set(v,this[v]);
                    }
            }
        });
        return rst;
    }

    /**
     * Set properties on object
     * @param data 
     */
    public setProperties(data: any){
        let key = '';
        for (key in data) {
            if(key in this){
                this[key] = data[key];
            }else{
                Object.defineProperty(this, key, {writable: true,value: data[key]});
            }
        }
    }
}
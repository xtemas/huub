import Product from './Product';

export default class ProductPage {
    protected _number: number;
    protected _products: Product[];
    protected _next: string;
    protected _previous: string;
    protected _total: number;
    constructor(number: number, next: string , previous: string, totalPages: number) {
        this._number = number;
        this._next = next;
        this._previous = previous;
        this._total = totalPages;
        this._products = [];
    }

    get number(): number {
        return this._number;
    }
    get next(): string {
        return this._next;
    }
    get previous(): string {
        return this._previous;
    }
    get totalPages(): number {
        return this._total;
    }
    get totalInPage(): number {
        return this._products.length;
    }
    get products(): Product[] {
        return this._products;
    }


    /**
     * Functions
     */

    /**
     * Add product object into collection
     * @param product 
     */
    public addProduct(product: Product): void{
        this._products.push(product);
    }

    /**
     * Find product on the page
     * @param id 
     * @returns 
     */
    public getProduct(id: number): Product|null{
        const prod = this.products.find( (p) => {
            return p.id == id;
        });
        return prod ? prod : null;
    }
}
import Variant from './Variant';

export default class Product {
    protected _id: number;
    protected _name: string;
    protected _description: string;
    protected _supplier: string;
    protected _season: string;
    protected _family: string;
    protected _subfamily: string;
    protected _type: string;
    protected _fabric: string;
    protected _currency: string;
    protected _variants: Variant[];

    /**
     * Constructor
     * @param id 
     */
    constructor(id: number) {
        this._id = id;
        this._name = '';
        this._description = '';
        this._supplier = '';
        this._season = '';
        this._family = '';
        this._subfamily = '';
        this._type = '';
        this._fabric = '';
        this._currency = 'EUR';
        this._variants = [];
    }
    
    get id(): number {
        return this._id;
    }

    get description(): string {
        return this._description;
    }
    set description(value: string ){
        this._description = value;
    }

    get name(): string {
        return this._name;
    }
    set name(value: string ){
        this._name = value;
    }

    get supplier(): string {
        return this._supplier;
    }
    set supplier(value: string ){
        this._supplier = value;
    }

    get season(): string {
        return this._season
    }
    set season(value: string ){
        this._season = value;
    }

    get family(): string {
        return this._family
    }
    set family(value: string ){
        this._family = value;
    }

    get subfamily(): string {
        return this._subfamily;
    }
    set subfamily(value: string ){
        this._subfamily = value;
    }

    get type(): string {
        return this._type
    }
    set type(value: string ){
        this._type = value;
    }

    get fabric(): string {
        return this._fabric;
    }
    set fabric(value: string ){
        this._fabric = value;
    }

    get currency(): string {
        return this._currency;
    }
    set currency(value: string ){
        this._currency = value;
    }

    get variants(): Variant[] {
        return this._variants;
    }
    set variants(value: Variant[] ){
        this._variants = value;
    }

    /**
     * functions
     * 
     * Set properties from object
     * @param data 
     */
    public loadByData(data: any){
        let key = '';
        for (key in data) {
            if(key == 'variants' || key == 'id'){
                continue;
            }
            if(key == 'currency_iso3code'){
                this.currency = data.currency_iso3code;
            }
            if((key in this)){
                this[key] = data[key];
            }
        }
        if('variants' in data && Array.isArray(data.variants)){
            const variants:Variant[] = [];
            data.variants.map( (variant:any) => {
                const v:Variant = new Variant(variant.ean,variant.reference);
                v.setProperties(variant);
                variants.push(v);
            });
            this.variants = variants;
        }
    }

    /**
     * Test if obj is equals to this object
     * @param obj 
     * @returns 
     */
    public equals(obj: Product){
        let key = '';
        for (key in obj) {
            if(this[key] != obj[key]){
                return false;
            }
        }
        return true;
    }
}
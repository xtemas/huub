import Vue from 'vue'
import Vuex from 'vuex'
import Api from '../models/ApiCall';
import VueSessionStorage from "vue-sessionstorage";
Vue.use(VueSessionStorage);
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    page: null,
    product: null,
    lastError: null,
    loading: false,
    token: null,
    expirationToken: new Date()
  },
  getters: {
    page: state => state.page,
    product: state => state.product,
    token: (state) => {
        const d = new Date();
        return state.expirationToken && state.expirationToken.getTime() >= d.getTime() ? state.token : null;
    },
    loading: state => state.loading,
    error: state => state.lastError
  },
  mutations: {
    error: (state, payload)  => {
        state.lastError = payload;
    },
    setToken: (state, payload)  => {
        state.token = payload.token;
        state.expirationToken = payload.expiration;
    },
    setPage: (state, page)  => {
        state.page = page;
    },
    setProduct: (state, product)  => {
        state.product = product;
    },
    setLoading: (state, value)  => {
        state.loading = value;
    },
    logout: (state)  => {
        state.token = null;
        Vue.prototype.$session.clear();
    }
  },
  actions: {
    setToken: (context, payload) => {
        if(payload.token){
            context.commit('setToken',payload);
            Vue.prototype.$session.set('token',payload.token);
            Vue.prototype.$session.set('expiration',payload.expiration);
        }
    },
    setProduct: async (context, product) => {
        if(!product){
            return context.commit('setProduct',null);
        }
        context.commit('setLoading',true);
        const data = await Api.getProduct(product.id,context.state.token);
        context.commit('setProduct',(data ? data : product));
        context.commit('setLoading',false);
    },
    loadToken: (context) => {
        const token =  Vue.prototype.$session.get('token');
        const expiration =  Vue.prototype.$session.get('expiration');
        if(token && expiration){
            context.commit('setToken',{
                token: token,
                expiration: new Date(expiration + '')
            });
        }
    },
    logout: (context, payload) => {
        context.commit('logout',payload);
    },
    loadPage: async (context, number) => {
        context.commit('setLoading',true);
        const data = await Api.getPage(number,context.state.token);
        if(data){
            context.commit('setPage',data);
        }else{
            const error = Api.lastError ? Api.lastError : 'Error, please try again!';
            context.commit('error',error);    
        }
        context.commit('setLoading',false);
    },
    error: (context, error) => {
        context.commit('error',error);
    }
  },
  modules: {}
})
